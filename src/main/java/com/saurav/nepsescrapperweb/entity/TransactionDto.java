package com.saurav.nepsescrapperweb.entity;

import java.time.LocalDate;

public class TransactionDto {

    private String transactionId;
    private String stockName;
    private String buyer;
    private String seller;
    private Integer quantity;
    private Integer rate;
    private LocalDate creationDate;
    private Integer indexPage;

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public TransactionDto(String transactionId, String stockName, String buyer, String seller, Integer quantity, Integer rate, LocalDate creationDate, Integer indexPage) {
        this.transactionId = transactionId;
        this.stockName = stockName;
        this.buyer = buyer;
        this.seller = seller;
        this.quantity = quantity;
        this.rate = rate;
        this.creationDate = creationDate;
        this.indexPage = indexPage;
    }

    public TransactionDto() {

    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Integer getIndexPage() {
        return indexPage;
    }

    public void setIndexPage(Integer indexPage) {
        this.indexPage = indexPage;
    }
}
