package com.saurav.nepsescrapperweb.repository;

import com.saurav.nepsescrapperweb.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<Transaction,Integer> {

}
