package com.saurav.nepsescrapperweb.controller;

import com.saurav.nepsescrapperweb.entity.Transaction;
import com.saurav.nepsescrapperweb.entity.TransactionDto;
import com.saurav.nepsescrapperweb.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/transactions")
public class NEPSEFloorSheetController {

    @Autowired
    TransactionRepository repository;

    @PostMapping
    public void saveFloorsheet(@RequestBody TransactionDto dto) {

        Transaction transaction = new Transaction();
        transaction.setBuyer(dto.getBuyer());
        transaction.setSeller(dto.getSeller());
        transaction.setQuantity(dto.getQuantity());
        transaction.setRate(dto.getRate());
        transaction.setTransactionId(dto.getTransactionId());
        transaction.setStockName(dto.getStockName());
        transaction.setCreationDate(dto.getCreationDate());
        transaction.setIndexPage(dto.getIndexPage());
        repository.save(transaction);
    }

}
