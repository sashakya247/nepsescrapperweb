package com.saurav.nepsescrapperweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NepsescrapperwebApplication {

	public static void main(String[] args) {
		SpringApplication.run(NepsescrapperwebApplication.class, args);
	}

}
